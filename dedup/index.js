require('dotenv').config();
require('module-alias/register');
const express = require('express');
const Db = require('@Class/Db');
const router = require('@Route');

const app = express();
const port = process.env.PORT || 8080;

(async () => {
	await Db.authenticate();
	await Db.initModels();

	app.use(router);
    
	app.listen(port, () => {
		console.log(`app listening at ${port}`);
	});
})();
