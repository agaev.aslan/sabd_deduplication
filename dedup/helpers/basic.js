const shuffleArray = (array) => {
	for (let i = array.length - 1; i > 0; i--) {
		const j = Math.floor(Math.random() * (i + 1));
		const temp = array[i];
		array[i] = array[j];
		array[j] = temp;
	}
	return array;
};
const randomLength = () => 30 * Math.random()*62|0 + 2;
	
const randomText = (length, basicOnly) => {
	const basic = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
	const additional = ' ,.?\n';
	const chars = basicOnly ? basic : additional + basic;
	for (var s = ''; s.length < length; s += chars.charAt(Math.random()*62|0));
	return s;
};

module.exports = {
	shuffleArray,
	randomLength,
	randomText,
};