const fs = require('fs');
const util = require('util');
const writeFile = util.promisify(fs.writeFile);
const readFile = util.promisify(fs.readFile);

module.exports = new class {
	async writeFile(...args) { return writeFile(...args); }
	async readFile(...args) { return readFile(...args); }
	createReadStream({ path, chunkSize }) { return fs.createReadStream(path, { highWaterMark: chunkSize, encoding: 'utf-8'}); }
	createWriteStream({ path }) { return fs.createWriteStream(path, { flags: 'a' }); }
	filesOfDir(path) { return fs.readdirSync(path).filter(file => fs.lstatSync(path+'/'+file).isFile()); }
};
