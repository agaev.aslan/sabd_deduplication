const { Sequelize, DataTypes } = require('sequelize');

module.exports = new class {
	constructor() {
		const dbHost = process.env.POSTGRES_HOST_DOCKER || process.env.POSTGRES_HOST;
		const connStr = `postgres://${process.env.POSTGRES_USER}:${process.env.POSTGRES_PASSWORD}@${dbHost}:5432/${process.env.POSTGRES_DB}`;
		this.sequelize = new Sequelize(connStr, {
			logging: false,
			pool: {
				max: 50,
				min: 0,
				acquire: 30000,
				idle: 10000
			},
		});
	}

	async authenticate() {
		try {
			await this.sequelize.authenticate();
			console.log('Connection has been established successfully.');
		} catch (error) {
			console.error('Unable to connect to the database:', error);
		}
	}

	async initModels() {
		this.Hash = this.sequelize.define('Hash', {
			id: {
				type: DataTypes.STRING,
				primaryKey: true, 
			},
			hash: {  type: DataTypes.STRING },
			filepath: { type: DataTypes.STRING },
			filepos: { type: DataTypes.INTEGER },
			file_id: { type: DataTypes.INTEGER },
		}, {
			freezeTableName: true,
		});

		this.Files = this.sequelize.define('Files', {
			id: {
				type: DataTypes.INTEGER,
				autoIncrement: true,
				primaryKey: true,
			},
			path: { type: DataTypes.STRING },
			filename: { type: DataTypes.STRING, unique: true },
		});

		this.Chunks = this.sequelize.define('Chunks', {
			id: {
				type: DataTypes.INTEGER,
				autoIncrement: true,
				primaryKey: true,
			},
			hash_id: { type: DataTypes.STRING },
			file_id: { type: DataTypes.INTEGER },
			filepath: { type: DataTypes.STRING },
			filepos: { type: DataTypes.INTEGER },
			size: { type: DataTypes.INTEGER },
			startInFile: { type: DataTypes.INTEGER },
			endInFile: { type: DataTypes.INTEGER },
		});


		await this.sequelize.sync({ force: true });
		console.log('All models were synchronized successfully.');
	}
    
};