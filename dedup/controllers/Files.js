const File = require('@Class/File');
const Db = require('@Class/Db');

const upload = async (req, res) => {
	const { filename } = req.query;
	const file = new File({ filename, db: Db, res });
	file.readInputStream().catch((e) => res.status(500).send(e));
};

const download = async (req, res) => {
	const { fileId } = req.query;

	const file = new File({ fileId: fileId, db: Db });
	const result = await file.readFile();
	res.status(200).send(result);
};

const list = async (req, res) => {
	const files = await Db.Files.findAll({ raw: true });
	res.status(200).send(files);
};

module.exports = {
	upload,
	download,
	list,
};