const path = require('path');
const Crypto = require('./Crypto');
const fs = require('./FsWrapper');

class File {
	constructor({ filename, fileId, db, res }) {
		this.db = db;
		// stored strings delimiter
		this.delimiter = '\naheeeble\n';
		this.crypto = new Crypto();
		this.res = res;
		// size of chunks to split data
		this.chunkSize = parseInt(process.env.CHUNK_SIZE);

		if (fileId) {
			this.fileId = fileId;
		}

		if (filename) {
			this.filename = filename;
			this.chunks = [];
			// input file path
			this.path = path.resolve(__dirname, '../data/input_files/', this.filename);
		}
	}

	// stored data path
	get localPath() { return path.resolve(__dirname, '../data/stored_files/', this.filename); }
 
	get randomNums() { return (Math.random() + Math.random() * Math.random() + Math.random() / Math.random()).toString(); }

	// reads data from data source (local file), splits and saves new data chunks in data storage (local files)
	async readInputStream() {
		this.transaction = await this.db.sequelize.transaction();
		this.readStream = fs.createReadStream({ path: this.path, chunkSize: this.chunkSize });
        
		this.readStream.on('data', async (chunk) => {
			this.chunks.push(chunk);
		});
        
		this.readStream.on('end', async () => {
			// for testing to avoid file overriding
			if (await this.db.Files.count({ where: { filename: this.filename } })) {
				this.filename = this.filename + (Math.random() + Math.random() / Math.random()).toString();
			}

			// create file for new data
			const file = await this.db.Files.create({
				filename: this.filename,
				path: this.localPath,
			}, { transaction: this.transaction });

			const newChunks = [];
			const dbChunks = [];

			const chunksData = new Map();
			const hashesSet = new Set();

			// calc hashes and select data chunks that are not present in the database
			this.chunks.forEach((chunk, i) => {
				const hash = this.crypto.getHash(chunk);
				hashesSet.add(hash);
				chunksData.set(i, { chunk, hash, i });
			});

			const existingHashes = await this.db.Hash.findAll({
				where: { hash: Array.from(hashesSet) },
				raw: true,
				transaction: this.transaction,
			});

			const hashesToCreate = [];
			this.chunks.forEach((chunk, i) => {
				const startInFile = i * this.chunkSize;
				const endInFile = startInFile + this.chunkSize - 1;

				const chunkData = chunksData.get(i);
				let dbChunk = {
					file_id: file.id,
					size: chunk.length,
					startInFile,
					endInFile,
				};
				let hash = existingHashes.find((hash) => hash.hash === chunkData.hash);
				if (!hash) {
					newChunks.push(chunk);
					const prevLen = newChunks.length;
					hash = {
						id: chunk.toString() + (new Date()).toISOString() + this.randomNums,
						file_id: file.id,
						hash: chunkData.hash,
						filepos: prevLen - 1,
						filepath: file.path,
					};

					hashesToCreate.push(hash); 
				}

				dbChunks.push({
					...dbChunk,
					hash_id: hash.id,
					filepath: hash.filepath,
					filepos: hash.filepos,
				});
			});


			await this.db.Hash.bulkCreate(hashesToCreate, { transaction: this.transaction });
			await this.db.Chunks.bulkCreate(dbChunks);

			if (newChunks?.length) {
				// writing new data chunks to local file
				await fs.writeFile(this.localPath, ''); 

				const writeStream = fs.createWriteStream({ path: this.localPath });

				newChunks.forEach((chunk) => {
					writeStream.write(chunk);
					writeStream.write(this.delimiter);
				});
				writeStream.end();
			}

			await this.transaction.commit();
            
			this.res.send('upload finished');
		});
        
		this.readStream.on('error', (err) => {
			console.log('fs read stream error :', err);
		});
	}

	// собирает из кусков нужный файл и отдает
	async readFile() {
		// get data chunks related to the file
		const chunks = await this.db.Chunks.findAll({
			where: { file_id: this.fileId },
			order: [ ['startInFile'] ],
			raw: true,
		});

		// read files containing required data chunks and accumulate
		const resData = []; 
		const files = new Map();
		for (let i = 0; i < chunks.length; i++) {
			const chunk = chunks[i];

			if (!files.has(chunk.filepath)) {
				const fileData = (await fs.readFile(chunk.filepath)).toString();
				files.set(chunk.filepath, fileData.split(this.delimiter));
			}

			const fileParts = files.get(chunk.filepath);
			resData.push({ data: Buffer.from(fileParts[chunk.filepos]), i });
		}

		const sortedData = resData.sort(((a, b) => {
			if (a.i > b.i) {
				return 1;
			}
			if (a.i < b.i) {
				return -1;
			}
			return 0;
		})).map((el) => el.data);

		return Buffer.concat(sortedData);
	} 
}

module.exports = File;