const router = require('express').Router();
const fileController = require('@Controller/Files');

router.get('/files/upload', fileController.upload);
router.get('/files/download', fileController.download);

module.exports = router;
