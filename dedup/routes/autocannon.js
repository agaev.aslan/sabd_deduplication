const router = require('express').Router();
const autocannonController = require('@Controller/Autocannon');

router.get('/autocannon/write', autocannonController.write);
router.get('/autocannon/read', autocannonController.read);
router.get('/autocannon/gen-files', autocannonController.genFiles);

module.exports = router;
