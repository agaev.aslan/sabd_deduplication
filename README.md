This is a system for data deduplication developed as a coursework for the course "big data analysis systems".

**Usage:**

docker-compose up

**Request**

http://localhost:8080/files/upload?filename=testin.txt


http://localhost:8080/files/download?fileId=1

**.env template**

```
POSTGRES_PASSWORD=
POSTGRES_USER=
POSTGRES_DB=
POSTGRES_HOST=
CHUNK_SIZE=
```

**Stress testing**

- http://localhost:8080/autocannon/gen-files

2000 input files containing random text will be generated.

- http://localhost:8080/autocannon/write

Autocannon library will be used to send http://localhost:8080/files/upload?filename={filename} requests for 60 seconds ({filename} is randomly selector file generated with /gen-files request)

- http://localhost:8080/autocannon/read

Autocannon library will be used to send http://localhost:8080/files/download?fileId={fileId} requests for 60 seconds 



