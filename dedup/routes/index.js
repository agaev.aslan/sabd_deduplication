const router = require('express').Router();
const files = require('./files');

router.use(function requestLog(req, res, next) {
	console.log('Incoming request: ', JSON.stringify({ path: req.path, method: req.method }, null, 2));
	next();
});

router.use(files);

module.exports = router;