const path = require('path');
const autocannon = require('autocannon');
const Db = require('@Class/Db');
const fs = require('@Class/FsWrapper');
const helper = require('@Helper/basic');

const FILES_NUM = 2000;

const write = async () => {
	let requests = fs.filesOfDir(path.resolve(__dirname, './data/input_files/')).map(file => ({
		path: 'http://localhost:8080/upload?filename=' + file,
		method: 'GET',
	}));
	requests = helper.shuffleArray(requests);

	if (!requests.length) {
		res.status(404).send('No files exist');
	}

	const instance = autocannon({
		url: 'http://localhost:8080',
		duration: 60,
		connections: 4,
		requests,
	}, console.log);
	
	process.once('SIGINT', () => {
		instance.stop();
	});
	
	autocannon.track(instance, {renderProgressBar: false});
};

const read = async (req, res) => {
	const files = await Db.Files.findAll({ raw: true });
	const requests = files.map((file) => ({
		path: 'http://localhost:8080/download?fileId=' + file.id,
		method: 'GET',
	}));

	if (!requests.length) {
		res.status(404).send('No files exist');
	}

	const instance = autocannon({
		url: 'http://localhost:8080',
		duration: 60,
		connections: 4,
		requests,
	}, console.log);
	
	process.once('SIGINT', () => {
		instance.stop();
	});
	
	autocannon.track(instance, {renderProgressBar: false});
};

const genFiles = async (req, res) => {
	const requests = [];

	await Promise.all(new Array(FILES_NUM).fill(1).map(async () => {
		const filename = helper.randomText(15, true) + '.txt';
		const filepath = path.resolve(__dirname, './data/input_files/', filename);
		const text = helper.randomText(helper.randomLength());
		await fs.writeFile(filepath, text);

		requests.push({ path: filename });
	}));

	res.status(200).send('files generated');
};


module.exports = {
	write,
	read,
	genFiles,
};